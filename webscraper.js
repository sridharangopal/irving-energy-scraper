import * as dotenv from 'dotenv'
dotenv.config()

import puppeteer from 'puppeteer'
import got from 'got'

async function send(feedKey, data) {
  const options = {
    url: 'https://io.adafruit.com/api/v2/' + process.env.ADAFRUIT_USERNAME + '/feeds/' + feedKey + '/data',
    method: 'POST',
    headers: {
      'User-Agent': 'Got',
      'X-AIO-Key': process.env.ADAFRUIT_AIO
    },
    json: {
      value: data
    }
  };

  const response = await got(options);
  if(response.statusCode !== 200) throw new Error('Send to Adafruit failed : ' + response.statusCode)
}

async function extract(data) {
  // Define regex patterns to match the desired values
  // const gallonsRegex = /([\d\s]+) gallons/;
  const percentRegex = /(\d+%)/;
  const dateRegex = /(\d{2}\/\d{2}\/\d{4})/;

  // Apply each regex pattern to extract the respective values
  // let matches = data.match(gallonsRegex);
  // if (matches && matches[1]) {
    // const gallons = parseInt(matches[1].replace(/(\s+) gallons/, ""));
    let percentRegexMatch = data.match(percentRegex);
    if (percentRegexMatch) {
      const percentRemaining = parseInt(percentRegexMatch[1]);
      let lastReadingDateMatch = data.match(dateRegex);
      if (lastReadingDateMatch) {
        const lastReadingDate = new Date(lastReadingDateMatch[1]).toLocaleDateString();
        // Store the extracted values in variables
        var extracted_data = {
          // gallons_remaining: gallons,
          percent_remaining: percentRemaining,
          last_reading_date: lastReadingDate
        }
      }
    }
  // }
  return extracted_data
}

async function run() {
  const browser = await puppeteer.launch({
    headless: true,
    timeout: 60000,
    args: ['--no-sandbox','--disable-setuid-sandbox']
  });
  const page = await browser.newPage();
  // dom element selectors
  const USERNAME_SELECTOR = '#email_check';
  const PASSWORD_SELECTOR = '#password_check';
  const BUTTON_SELECTOR = '#cmdLogin';
  const PRICE_SELECTOR = '#tank-data-cage > div.bootstrap-table.bootstrap4 > div.fixed-table-container.has-card-view > div.fixed-table-body > table > tbody > tr > td > div > div:nth-child(5) > span.card-view-value.undefined'
  const TANK_GUAGE_SUMMARY = '#tank_gauge_summary'

  await page.goto('https://myaccount.irvingenergy.com/login');
  await page.click(USERNAME_SELECTOR);
  await page.keyboard.type(process.env.IRVING_USERNAME);
  await page.click(PASSWORD_SELECTOR);
  await page.keyboard.type(process.env.IRVING_PASSWORD);
  await page.click(BUTTON_SELECTOR);
  await page.waitForSelector(PRICE_SELECTOR);

  let price = await page.evaluate((sel) => {
    return document.querySelector(sel).outerText;
  }, PRICE_SELECTOR);
  console.log('Price : ' + price);

  let tank_guage_summary = await page.evaluate((sel) => {
    return document.querySelector(sel).outerText
  }, TANK_GUAGE_SUMMARY);
  console.log(tank_guage_summary)
  var extracted_data = await extract(tank_guage_summary)
  console.log(extracted_data)

  await page.goto('https://myaccount.irvingenergy.com/logout')

  await send('propaneprice', price);
  await send('propanelevel', extracted_data.percent_remaining);
  // await send('gallonsremaining', extracted_data.gallons_remaining);
  await send('lastreadingdate', extracted_data.last_reading_date);

  await browser.close();
}

// main body
run();
